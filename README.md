![Build Status](https://gitlab.com/pages/plain-html/badges/master/build.svg)

---

# ml-telegb - Experimental Home Page

---

## GitLab Pages

This project is a [website on the internet](https://knbknb.gitlab.io/ml-telegb-html).

The code is [hosted at git.gfz-potsdam.de](https://git.gfz-potsdam.de/knb/ml-telegb-html), but deployment to the public internet takes place at gitlab.com.


#### Under construction: 

I had to  rename the project to `knbknb.gitlab.io`. There is no site here.

The actual site is at  [`knbknb.gitlab.io/ml-telegb-html`](https://knbknb.gitlab.io/ml-telegb-html) (under construction) and this repo is mirrored at [gitlab.com/knbknb/ml-telegb-html](https://gitlab.com/knbknb/ml-telegb-html).

Read more about [user/group Pages][userpages] and [project Pages][projpages].

Learn more about GitLab Pages at https://pages.gitlab.io and the official
documentation https://docs.gitlab.com/ce/user/project/pages/.

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml).

## Troubleshooting

(tbc)

[ci]: https://about.gitlab.com/gitlab-ci/
[index.html]: https://gitlab.com/pages/plain-html/blob/master/public/index.html
[userpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#user-or-group-pages
[projpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#project-pages
